<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Jkrug\Cache\Model;

use OxidEsales\Eshop\Core\Registry;

class Content extends Content_parent
{

    public function save()
    {
        parent::save();

        $oConfig     = Registry::getConfig();
        $sCompileDir = $oConfig->getConfigParam('sCompileDir');
        $sCacheDir   = $sCompileDir . "/jkrug_cache/";
        $md5Oxid     = md5($oConfig->getRequestParameter('oxid'));
        $matches     = glob($sCacheDir . 'ContentController_*' . $md5Oxid);

        if ($md5Oxid && is_array($matches)) {
            $i = 0;
            foreach ($matches as $match) {
                unlink($match);
                $i++;
            }
            if ($oConfig->isAdmin() && $i) {
                echo $i . ' cached file(s) deleted';
            }
        }
    }
}
