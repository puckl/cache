<?php

/**
 * This file is part of a free OXID eShop module.
 * It is Open Source - feel free to use it! But PLEASE guys:
 * Respect the author and keep the stuff correct.
 *
 * Version:    2.0
 * Author:     Joscha Krug <support@makaira.io>
 * Author URI: https://www.makaira.io
 */

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = [
    'id'            => 'jkrug/cache',
    'title'         => 'Joscha Krug :: OXID Cache',
    'description'   => 'Get a better performance by caching some stuff and tweaking the performance.',
    // ToDo: Add Image
    'thumbnail'     => 'cookbook.jpg',
    'version'       => '2.1.5',
    'author'        => 'Joscha Krug, Josef A. Puckl | eComStyle.de',
    'url'           => 'https://www.makaira.io',
    'email'         => 'jk@makaira.io',
    'extend' => [
        \OxidEsales\Eshop\Core\ShopControl::class           => Jkrug\Cache\Core\ShopControl::class,
        \OxidEsales\Eshop\Core\Session::class               => Jkrug\Cache\Core\Session::class,
        \OxidEsales\Eshop\Application\Model\Article::class  => Jkrug\Cache\Model\Article::class,
        \OxidEsales\Eshop\Application\Model\Category::class => Jkrug\Cache\Model\Category::class,
        \OxidEsales\Eshop\Application\Model\Content::class  => Jkrug\Cache\Model\Content::class,
    ],
    'settings' => [
        [
            'group' => 'main',
            'name'  => 'iCacheLifetime',
            'type'  => 'str',
            'value' => '28800'
        ],
        [
            'group' => 'main',
            'name'  => 'aCachedClasses',
            'type'  => 'arr',
            'value' => ['OxidEsales\Eshop\Application\Controller\StartController',
                        'OxidEsales\Eshop\Application\Controller\ArticleListController',
                        'OxidEsales\Eshop\Application\Controller\ArticleDetailsController',
                        'OxidEsales\Eshop\Application\Controller\ContentController'],
        ],
        [
            'group' => 'main',
            'name'  => 'bHtmlMinify',
            'type'  => 'bool',
            'value' => 'true'
        ],
        [
            'group' => 'main',
            'name'  => 'sCsrfMailto',
            'type'  => 'str',
            'value' => ''
        ],
    ]
];
