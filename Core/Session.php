<?php

namespace Jkrug\Cache\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Email;

class Session extends Session_parent
{

    public function checkSessionChallenge()
    {
        $ret = parent::checkSessionChallenge();
        if (!$ret && !$this->isAdmin()) {
            $this->_sendCsrfMail();
        }
        return $ret;
    }

    protected function _sendCsrfMail()
    {
        if ($csrfmail = Registry::getConfig()->getConfigParam('sCsrfMailto')) {
            $subject = Registry::getLang()->translateString('EXCEPTION_NON_MATCHING_CSRF_TOKEN');
            $body    = 'Session: ' . $this->getSessionChallengeToken() . ', Request: ' . $this->getRequestChallengeToken();
            $oEmail  = oxNew(Email::class);
            $oEmail->sendEmail($csrfmail, $subject, $body);
        }
        return;
    }
}
