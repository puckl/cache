<?php

/**
 * This file is part of a free OXID eShop module.
 * It is Open Source - feel free to use it! But PLEASE guys:
 * Respect the author and keep this note.
 *
 * Version:    2.0
 * Author:     Joscha Krug <support@makaira.io>
 * Author URI: https://www.makaira.io
 */

namespace Jkrug\Cache\src;

use \OxidEsales\Eshop\Core\Registry;
use \OxidEsales\Eshop\Core\Module\Module;
use \OxidEsales\Eshop\Application\Model\User;

class base_html_cache
{

    //ToDo: make widgets configurable separate to skip some of them with full basket.
    protected $_aCachableControllers = [];

    protected $_cacheBackend = null;

    public function __construct($oCacheBackend)
    {
        $this->_cacheBackend         = $oCacheBackend;
        $this->_aCachableControllers = $this->getCachableControllers();
    }

    /**
     * checks for a valid cache and if found, outputs it and skips the other rendering
     */
    public function getCacheContent()
    {
        if (!$this->isCachableRequest()) {
            return false;
        }

        $key      = $this->getCacheKey();
        $sContent = $this->_cacheBackend->getCache($key);

        if (is_string($sContent)) {
            return $sContent;
        }

        return false;
    }

    /**
     * Minify the html output
     *
     * ToDo: Make configurable -> DONE!
     *
     * @param $sValue string
     * @return string
     */
    protected function _minifyHtml($sValue)
    {
        $aSearch   = ['/ {2,}/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'];
        $aReplace  = [' ', ' '];
        $sMinified = preg_replace($aSearch, $aReplace, $sValue);

        return $sMinified;
    }

    /**
     * Adds the default OXID version Tags.
     * Please keep this function with respect for OXID!
     *
     * @param $sOutput
     * @return full content string
     */
    public function addCacheVersionTags($sOutput)
    {
        $oConf = Registry::getConfig();
        // DISPLAY IT
        $sVersion  = $oConf->getVersion();
        $sEdition  = $oConf->getFullEdition();
        $sCurYear  = date("Y");
        $sShopMode = "";

        // SHOW ONLY MAJOR VERSION NUMBER
        $aVersion      = explode('.', $sVersion);
        $sMajorVersion = reset($aVersion);

        // Replacing only once per page
        $sOutput = str_ireplace("</head>", "</head>\n  <!-- OXID eShop {$sEdition}, Version {$sMajorVersion}{$sShopMode}, Shopping Cart System (c) OXID eSales AG 2003 - {$sCurYear} - https://www.oxid-esales.com -->", ltrim($sOutput));

        return $sOutput;
    }

    /**
     * Create the Cache
     *
     * @param $sContent fully rendered content
     */
    public function createCache($sContent)
    {
        if (!$this->isCachableRequest()) {
            return;
        }
        $oConf = Registry::getConfig();

        $sContent = $this->_replaceStoken($sContent);

        if ($oConf->getConfigParam('bHtmlMinify')) {
            $sContent = $this->_minifyHtml($sContent);
            $sContent = $this->addCacheVersionTags($sContent);
        }

        if (false == $oConf->isUtf()) {
            $sCharset = Registry::getLang()->translateString('charset');
            $sContent = mb_convert_encoding($sContent, 'UTF-8', $sCharset);
        }

        if (!$oConf->isProductiveMode()) {
            //$span = '<span style="position:fixed;top:50px;left:50px;border:1px solid;padding:5px;">CACHED</span>';
            $sContent = str_ireplace("- https://www.oxid-esales.com -->", "- https://www.oxid-esales.com (from Cache) -->" . $span, ltrim($sContent));
        }

        $key = $this->getCacheKey();
        $this->_cacheBackend->setCache($key, $sContent);
    }

    /**
     * Check if this request could be cached.
     *
     * @return bool
     */
    public function isCachableRequest()
    {
        if (!in_array($this->getClassName(), $this->_aCachableControllers)) {
            return false;
        }
        $oConf = Registry::getConfig();

        //Check if any filter from makaira module has been set!
        if ($this->sClassName == 'OxidEsales\Eshop\Application\Controller\ArticleListController') {
            //OXID standard session filter
            $aSessionFilter = Registry::getSession()->getVariable('session_attrfilter');
            $sActCat        = $oConf->getRequestParameter('cnid');
            $SessfilActCat  = $aSessionFilter[$sActCat];
            $aAttrFilter    = (is_array($SessfilActCat)) ? array_filter($SessfilActCat) : 0;
            if (!empty($aAttrFilter)) {
                return false;
            }
            //Makaira filter
            //we need to use method_exists as there is a bug in oxmodule::isActive() prior OXID 4.7.11
            if (method_exists(Registry::get('oxViewConfig'), 'getAggregationFilter')) {
                $oxModule = oxNew(Module::class);
                $oxModule->load('makaira/connect');
                if ($oxModule->isActive()) {
                    $aFilter = array_filter(Registry::get('oxViewConfig')->getAggregationFilter());
                    if (!empty($aFilter)) {
                        return false;
                    }
                }
            }
        }
        //END Filter check

        if ($this->sFunction) {
            return false;
        }
        $oUser = oxNew(User::class);
        if ($oUser->loadActiveUser() !== false) {
            return false;
        }
        $partial = $oConf->getRequestParameter('renderPartial');
        if (!empty($partial)) {
            return false;
        }
        if ($this->_hasBasketItems()) {
            return false;
        }

        return true;
    }

    public function getClassName()
    {
        if (isset($this->sClassName)) {
            return $this->sClassName;
        }
        $oConf            = Registry::getConfig();
        $oActView         = $oConf->getActiveView();
        $sClassName       = $oActView->getClassName();
        $this->sClassName = $sClassName;

        return $this->sClassName;
    }

    /**
     * I guess we should refactor this.
     * Anway the extending classes inject data via this. :-/
     *
     * @deprecated
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Calclulate the Cache Key
     *
     * @return string cache Key
     */
    public function getCacheKey()
    {
        //ToDo: Change this for caching widgets
        $oUtilsServer = Registry::get("oxUtilsServer");
        $requestUrl   = $oUtilsServer->getServerVar("REQUEST_URI");

        //Parameter entfernen, da Datei aufgr. Parameter (?rand=1) mehrfach angelegt wurde
        $requestUrl = str_replace('?rand=1', '', $requestUrl);

        //Für Ecs B2B Modul, damit Shop je nach Kunde mit oder ohne Preise gezeigt wird.
        $oxModule = oxNew(Module::class);
        $oxModule->load('ecs_easyb2b');
        if ($oxModule->isActive()) {
            if ($b2bvar = $this->_isEcsB2BSessionRunning()) {
                $requestUrl = $requestUrl . $b2bvar;
            }
        }

        //Für Cookie Banner ECS
        $bannerCookie = $oUtilsServer->getOxCookie('consent');
        // Cookie Banner Aggrosoft
        $bannerCookie .= $oUtilsServer->getOxCookie('cc-categories');
        $bannerCookie .= $oUtilsServer->getOxCookie('cc-set');
        if (is_array($bannerCookie)) {
            $bannerCookie = implode("", html_entity_decode($bannerCookie));
        }

        if ($bannerCookie) {
            $requestUrl = $requestUrl . 'cc_' . $bannerCookie;
        }

        if ($lang = Registry::getConfig()->getRequestParameter('lang')) {
            $requestUrl = $requestUrl . $lang;
        }

        $ClassName = str_replace('OxidEsales\Eshop\Application\Controller\\', '', $this->getClassName());
        $key       = $ClassName . '_' . md5($requestUrl);

        // hänge noch die oxid an, um die Dateien einzeln beim Speichern von Änderungen löschen zu können
        if ($ClassName == 'ArticleDetailsController' && $sArtOxid = Registry::getConfig()->getRequestParameter('anid')) {
            $key = $key . '_' . md5($sArtOxid);
        }
        if ($ClassName == 'ArticleListController' && $sCatOxid = Registry::getConfig()->getRequestParameter('cnid')) {
            $key = $key . '_' . md5($sCatOxid);
        }
        if ($ClassName == 'ContentController' && $sContOxid = Registry::getConfig()->getRequestParameter('oxcid')) {
            $key = $key . '_' . md5($sContOxid);
        }
        return $key;
    }

    /**
     * Check if there are items in the basket which will lead to a non cachable request.
     * //ToDo: this could be skipped for caching most of the widgets. right?!
     *
     * @return bool
     */
    protected function _hasBasketItems()
    {
        $oBasket = Registry::getSession()->getBasket();
        if ($oBasket && $oBasket->getProductsCount() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Returns cachable controllers according to backend settings
     *
     * @return array
     */
    protected function getCachableControllers()
    {
        return Registry::getConfig()->getShopConfVar('aCachedClasses', null, 'module:jkrug/cache');
    }

    /**
     * Für Ecs B2B Modul, damit Shop je nach Kunde mit oder ohne Preise gezeigt wird.
     */
    protected function _isEcsB2BSessionRunning()
    {
        $oSession = Registry::getSession();
        if ($oSession and $oSession->hasVariable('b2bsession')) {
            if ($var = $oSession->getVariable('b2bsession')) {
                return $var;
            }
        }
        $b2bcookie = Registry::getUtilsServer()->getOxCookie('b2b');
        if ($cookie = $b2bcookie) {
            return $cookie;
        }
        return false;
    }

    protected function _replaceStoken($sContent)
    {
        if ($sToken = Registry::getSession()->getSessionChallengeToken()) {
            $sInputToken = "<input type=\"hidden\" name=\"stoken\" value=\"" . $sToken . "\" />";
            $sContent    = str_replace($sInputToken, '', $sContent);
        }
        $sPlaceholder = "<input type=\"hidden\" name=\"__STOKENNAME__\" value=\"__STOKENVALUE__\" />";
        $sSearch      = "<input type=\"hidden\" name=\"cl\"";
        $sReplace     = $sPlaceholder . $sSearch;
        return str_replace($sSearch, $sReplace, $sContent);
    }
}
